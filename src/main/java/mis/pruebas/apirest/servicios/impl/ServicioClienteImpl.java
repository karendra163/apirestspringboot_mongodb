package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

@Service

public class ServicioClienteImpl implements ServicioCliente {
    @Autowired
    RepositorioCliente repositorioCliente;
    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public Page<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(documento);
        if (!quizasCliente.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if (!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + cliente.documento);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {
        throw new UnsupportedOperationException("NO IMPLEMANTADO");
    }

    @Override
    public void borrarCliente(String documento) {
        this.repositorioCliente.deleteByDocumento(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = obtenerCliente(documento);
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);
        cliente.codigosCuentas.add(numeroCuenta);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.codigosCuentas;
    }
}
