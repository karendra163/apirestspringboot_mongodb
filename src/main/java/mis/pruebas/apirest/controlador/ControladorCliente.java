package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;
    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    @GetMapping
    public PagedModel<EntityModel<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            final var pageSinMetadatos = this.servicioCliente.obtenerClientes(pagina, cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                                    .withSelfRel().withTitle("Este cliente"),
                            linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(x.documento))
                                    .withRel("cuentas").withTitle("Cuentas del cliente")
                    ));

            return this.pagedResourcesAssembler.toModel(page);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public void agregarCliente(@RequestBody Cliente cliente) {
        this.servicioCliente.insertarClienteNuevo(cliente);
    }

    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento) {
        try {
            System.err.println(String.format("obtenerUnCliente %s", documento));
            return this.servicioCliente.obtenerCliente(documento);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{documento}")
    public void emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.emparcharCliente(cliente);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch(ObjetoNoEncontrado x) {}
    }
}
