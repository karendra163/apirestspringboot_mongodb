package mis.pruebas.apirest.controlador;


import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}

    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta){this.servicioCuenta.insertarCuentaNueva(cuenta);}

    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        try {
            return this.servicioCuenta.obtenerCuenta(numero);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.guardarCuenta(cuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.emparcharCuenta(cuenta);
        }catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (ObjetoNoEncontrado x) {
        }
    }

}
